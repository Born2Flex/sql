package org.practice;

import com.github.javafaker.Faker;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.*;
import org.practice.connection.DBConnector;

import java.sql.*;
import java.util.Locale;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class DBInitializerH2Test {
    private static Faker faker;
    private Properties properties;
    private static Validator validator;
    private static DBConnector connector;
    private static Connection connection;
    private static Savepoint savepoint;

    @BeforeAll
    static void setup() throws SQLException {
        faker = new Faker(new Locale("uk"));
        validator = Validation.buildDefaultValidatorFactory().getValidator();
        Properties properties = new Properties();
        properties.setProperty("url", "jdbc:h2:mem:testdb;MODE=PostgreSQL;LOCK_TIMEOUT=1000");
        properties.setProperty("username", "sa");
        properties.setProperty("password", "");
        connector = new DBConnector(properties);
        connection = connector.connect();
        connection.prepareStatement("DROP TABLE IF EXISTS addresses, categories, shops, products, product_shop;" +
                "CREATE TABLE categories (\n" +
                "    category_id   SERIAL PRIMARY KEY,\n" +
                "    category_name VARCHAR(50) NOT NULL\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE products (\n" +
                "    product_id   SERIAL PRIMARY KEY,\n" +
                "    category_id  INTEGER REFERENCES categories (category_id),\n" +
                "--     shop_id INTEGER REFERENCES shops (shop_id),\n" +
                "    product_name VARCHAR(50) NOT NULL\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE shops (\n" +
                "    shop_id    SERIAL PRIMARY KEY,\n" +
                "    city       VARCHAR(40) NOT NULL,\n" +
                "    street     VARCHAR(40) NOT NULL,\n" +
                "    house      VARCHAR(10) NOT NULL\n" +
                ");").executeUpdate();
        connection.setAutoCommit(false);
        savepoint = connection.setSavepoint("init");
    }

    @BeforeEach
    void setUp() throws SQLException {
        properties = mock(Properties.class);
        connection = connector.connect();
        connection.setAutoCommit(false);
    }

    @AfterEach
    void tearDown() throws SQLException {
        connection.rollback(savepoint);
        connection.close();
    }

    @Test
    void testDBInitializer_GeneratingCategories() throws SQLException {
        clearAllTables();
        DBInitializer.ProductsInitializer initializer =
                new DBInitializer(connector, properties, faker).new ProductsInitializer(connector.connect());
        assertDoesNotThrow(() -> {
            initializer.generateCategories(50, connection.prepareStatement(initializer.addCategorySQL));
            try (ResultSet result = connection.prepareStatement("SELECT COUNT(*) FROM categories").executeQuery()) {
                assertTrue(result.next(), "Query result should have one row");
                int rowCount = result.getInt(1);
                assertEquals(50, rowCount, "The 'categories' table should have 50 rows");
            }
        });
    }

    @Test
    void testDBInitializer_GeneratingProducts() {
        clearAllTables();
        DBInitializer.ProductsInitializer initializer =
                new DBInitializer(connector, properties, faker).new ProductsInitializer(connector.connect());
        assertDoesNotThrow(() -> {
            initializer.generateCategories(10, connection.prepareStatement(initializer.addCategorySQL));
            initializer.generateProducts(25, 10, connection.prepareStatement(initializer.addProductSQL));
            try (ResultSet result = connection.prepareStatement("SELECT COUNT(*) FROM products").executeQuery()) {
                assertTrue(result.next(), "Query result should have one row");
                int rowCount = result.getInt(1);
                assertEquals(25, rowCount, "The 'products' table should have 25 rows");
            }
        });
    }

    @Test
    void testDBInitializer_GeneratingShops() {
        clearAllTables();
        DBInitializer.ShopsInitializer initializer =
                new DBInitializer(connector, properties, faker).new ShopsInitializer(connector.connect());
        assertDoesNotThrow(() -> {
            initializer.generateShops(100, connection.prepareStatement(initializer.addShopSQL));
            try (ResultSet result = connection.prepareStatement("SELECT COUNT(*) FROM shops").executeQuery()) {
                assertTrue(result.next(), "Query result should have one row");
                int rowCount = result.getInt(1);
                assertEquals(100, rowCount, "The 'products' table should have 25 rows");
            }
        });
    }

    @Test
    void testDBInitializer_Run() {
        Properties properties = new Properties();
        properties.setProperty("numOfCategories", "100");
        properties.setProperty("numOfProducts", "1000");
        properties.setProperty("numOfShops", "500");
        DBInitializer dbInitializer = new DBInitializer(connector, properties, faker);
        assertDoesNotThrow(() -> {
            dbInitializer.run();
            try (ResultSet resultShop = connection.prepareStatement("SELECT COUNT(*) FROM shops").executeQuery();
                 ResultSet resultProd = connection.prepareStatement("SELECT COUNT(*) FROM products").executeQuery();
                 ResultSet resultCat = connection.prepareStatement("SELECT COUNT(*) FROM categories").executeQuery()) {
                assertTrue(resultShop.next(), "Query result should have one row");
                int rowCount = resultShop.getInt(1);
                assertEquals(500, rowCount, "The 'products' table should have 500 rows");

                assertTrue(resultProd.next(), "Query result should have one row");
                int rowCountProd = resultProd.getInt(1);
                assertEquals(1000, rowCountProd, "The 'products' table should have 1000 rows");

                assertTrue(resultCat.next(), "Query result should have one row");
                int rowCountCat = resultCat.getInt(1);
                assertEquals(100, rowCountCat, "The 'categories' table should have 20 rows");
            }
        });
    }
    void clearAllTables() {
        try (Statement statement = connection.createStatement()) {
            clearTable(statement, "products");
            clearTable(statement, "categories");
            clearTable(statement, "shops");
        } catch (SQLException e) {
            throw new RuntimeException("Error truncating tables", e);
        }
    }
    private void clearTable(Statement statement, String tableName) throws SQLException {
        String sql = "DELETE FROM " + tableName;
        statement.executeUpdate(sql);
    }
}
