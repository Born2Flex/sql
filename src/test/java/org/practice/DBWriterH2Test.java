package org.practice;

import org.junit.jupiter.api.*;
import org.practice.connection.DBConnector;

import java.sql.*;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

public class DBWriterH2Test {
    private static DBConnector connector;
    private static Connection connection;
    private static Savepoint savepoint;

    @BeforeAll
    static void setup() throws SQLException {
        Properties properties = new Properties();
        properties.setProperty("url", "jdbc:h2:mem:testdb;MODE=PostgreSQL;LOCK_TIMEOUT=1000");
        properties.setProperty("username", "sa");
        properties.setProperty("password", "");
        connector = new DBConnector(properties);
        connection = connector.connect();
        connection.prepareStatement("DROP TABLE IF EXISTS addresses, categories, shops, products, product_shop;" +
                "CREATE TABLE categories (\n" +
                "    category_id   SERIAL PRIMARY KEY,\n" +
                "    category_name VARCHAR(50) NOT NULL\n" +
                ");\n" +
                "CREATE TABLE products (\n" +
                "    product_id   SERIAL PRIMARY KEY,\n" +
                "    category_id  INTEGER REFERENCES categories (category_id),\n" +
                "--     shop_id INTEGER REFERENCES shops (shop_id),\n" +
                "    product_name VARCHAR(50) NOT NULL\n" +
                ");\n" +
                "CREATE TABLE shops (\n" +
                "    shop_id    SERIAL PRIMARY KEY,\n" +
                "    city       VARCHAR(40) NOT NULL,\n" +
                "    street     VARCHAR(40) NOT NULL,\n" +
                "    house      VARCHAR(10) NOT NULL\n" +
                ");\n" +
                "CREATE TABLE product_shop (\n" +
                "    product_shop_id SERIAL PRIMARY KEY,\n" +
                "    shop_id         INTEGER REFERENCES shops (shop_id) NOT NULL,\n" +
                "    product_id      INTEGER REFERENCES products (product_id) NOT NULL,\n" +
                "    amount          INTEGER NOT NULL\n" +
                ");").executeUpdate();
        connection.setAutoCommit(false);
        fillTables();
        savepoint = connection.setSavepoint("init");
    }

    private static void fillTables() throws SQLException {
        PreparedStatement categories = connection.prepareStatement("INSERT INTO categories (category_name) VALUES (?)");
        PreparedStatement products = connection.prepareStatement("INSERT INTO products (category_id, product_name) VALUES (?, ?)");
        PreparedStatement shops = connection.prepareStatement("INSERT INTO shops (shop_id, city, street, house) VALUES (?, ?, ?, ?)");
        categories.setString(1, "Design");
        categories.addBatch();
        categories.executeBatch();
        connection.commit();
        for (int i = 1; i <= 100; i++) {
            products.setInt(1, 1);
            products.setString(2, "product");
            shops.setInt(1, i);
            shops.setString(2, "asd");
            shops.setString(3, "edfg");
            shops.setString(4, "abc");
            products.addBatch();
            shops.addBatch();
        }
        products.executeBatch();
        shops.executeBatch();
        connection.commit();
    }

    @BeforeEach
    void setUp() throws SQLException {
        connection = connector.connect();
        connection.rollback(savepoint);
        connection.setAutoCommit(false);
    }

    @AfterEach
    void cleanUp() {
    }

    @Test
    void testDBWriter_GenerateRecords_SingleThread() throws SQLException {
        clearTable(connection.createStatement(),"product_shop");
        int numOfRecords = 10999;
        int numOfProducts = 100;
        int numOfShops = 100;
        DBWriter.DBThread thread = new DBWriter(connector, new Properties()).new DBThread(connection, numOfRecords, numOfShops, numOfProducts);
        assertDoesNotThrow(() -> {
            thread.run();
            connection = connector.connect();
            try (ResultSet result = connection.prepareStatement("SELECT COUNT(*) FROM product_shop").executeQuery()) {
                assertTrue(result.next(), "Query result should have one row");
                int rowCount = result.getInt(1);
                assertEquals(10999, rowCount, "The 'product_shop' table should have 10999 rows");
            }
        });
    }

    @Test
    void testDBWriter_GenerateRecords_MultiThread() {
        Properties properties = new Properties();
        properties.setProperty("numOfThreads", "3");
        properties.setProperty("numOfCategories", "1");
        properties.setProperty("numOfProducts", "100");
        properties.setProperty("numOfShops", "100");
        properties.setProperty("numOfProductShopRecords", "30000");

        DBWriter writer = new DBWriter(connector, properties);
        assertDoesNotThrow(() -> {
            writer.run();
            connection = connector.connect();
            try (ResultSet result = connection.prepareStatement("SELECT COUNT(*) FROM product_shop").executeQuery()) {
                assertTrue(result.next(), "Query result should have one row");
                int rowCount = result.getInt(1);
                assertEquals(30000, rowCount, "The 'product_shop' table should have 10999 rows");
            }
        });
    }

    private void clearTable(Statement statement, String tableName) throws SQLException {
        String sql = "DELETE FROM " + tableName;
        statement.executeUpdate(sql);
    }
}
