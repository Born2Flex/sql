package org.practice.generators;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProductGeneratorTest {
    @Test
    void testTimeOfGeneration() {
        Faker faker = new Faker();
        int numOfCategories = 100;
        ProductGenerator productGenerator = new ProductGenerator(faker, numOfCategories);
        assertTimeout(Duration.ofSeconds(1L), () -> {
            Stream.generate(productGenerator::generateProduct)
                    .limit(10_000);
        });
    }
}