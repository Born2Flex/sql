package org.practice.generators;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ShopGeneratorTest {
    @Test
    void testTimeOfGeneration() {
        Faker faker = new Faker();
        ShopGenerator productGenerator = new ShopGenerator(faker);
        assertTimeout(Duration.ofSeconds(1L), () -> {
            Stream.generate(productGenerator::generateShop)
                    .limit(10000);
        });
    }
}