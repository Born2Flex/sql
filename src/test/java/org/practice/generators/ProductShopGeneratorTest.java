package org.practice.generators;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ProductShopGeneratorTest {

    @Test
    void testTimeOfGeneration() {
        int numOfProducts = 10000;
        int numOfShops = 10000;
        ProductShopGenerator productGenerator = new ProductShopGenerator(numOfShops, numOfProducts);
        assertTimeout(Duration.ofSeconds(1L), () -> {
            Stream.generate(productGenerator::generateProductShopRecord)
                    .limit(10000);
        });
    }

}