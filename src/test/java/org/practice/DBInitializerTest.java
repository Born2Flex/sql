package org.practice;

import com.github.javafaker.Faker;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.practice.connection.DBConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DBInitializerTest {
    private static Faker faker;
    private Properties properties;
    private static Validator validator;
    private static DBConnector connector;

    @BeforeAll
    static void setup() {
        faker = mock(Faker.class);
        validator = Validation.buildDefaultValidatorFactory().getValidator();
//        validator = mock(Validator.class);
        connector = mock(DBConnector.class);
    }

    @BeforeEach
    void prepareProperties() {
        properties = mock(Properties.class);
    }

    @Test
    void testProductInitializer_GenerateCategories() throws SQLException {
        faker = new Faker();
        Connection connection = mock(Connection.class);
        PreparedStatement addCategory = mock(PreparedStatement.class);

        DBInitializer.ProductsInitializer productsInitializer =
                new DBInitializer(connector, properties, faker).new ProductsInitializer(connection);
        assertDoesNotThrow(() -> {
            productsInitializer.generateCategories(5, addCategory);
        });
        verify(addCategory, times(5)).addBatch();
        verify(addCategory, times(1)).executeBatch();
        faker = mock(Faker.class);
    }

    @Test
    void testProductInitializer_GenerateCategories_ShouldThrow() throws SQLException {
        faker = new Faker();
        Connection connection = mock(Connection.class);
        PreparedStatement addCategory = mock(PreparedStatement.class);
        when(addCategory.executeBatch()).thenThrow(new SQLException());

        DBInitializer.ProductsInitializer productsInitializer =
                new DBInitializer(connector, properties, faker).new ProductsInitializer(connection);
        assertThrows(RuntimeException.class, () -> {
            productsInitializer.generateCategories(5, addCategory);
        });
        verify(addCategory, times(5)).addBatch();
        verify(addCategory, times(1)).executeBatch();
        faker = mock(Faker.class);
    }

    @Test
    void testProductInitializer_GenerateProducts() throws SQLException {
        faker = new Faker(new Locale("uk"));
        Connection connection = mock(Connection.class);
        PreparedStatement addProduct = mock(PreparedStatement.class);

        DBInitializer.ProductsInitializer productsInitializer =
                new DBInitializer(connector, properties, faker).new ProductsInitializer(connection);
        assertDoesNotThrow(() -> {
            productsInitializer.generateProducts(125, 15, addProduct);
        });
        verify(addProduct, times(125)).addBatch();
        verify(addProduct, times(2)).executeBatch();
        faker = mock(Faker.class);
    }

    @Test
    void testProductInitializer_GenerateProducts_ShouldThrow() throws SQLException {
        faker = new Faker(new Locale("uk"));
        Connection connection = mock(Connection.class);
        PreparedStatement addProduct = mock(PreparedStatement.class);
        when(addProduct.executeBatch()).thenThrow(new SQLException());

        DBInitializer.ProductsInitializer productsInitializer =
                new DBInitializer(connector, properties, faker).new ProductsInitializer(connection);
        assertThrows(RuntimeException.class, () -> {
            productsInitializer.generateProducts(99, 25, addProduct);
        });
        verify(addProduct, times(99)).addBatch();
        verify(addProduct, times(1)).executeBatch();
        faker = mock(Faker.class);
    }

    @Test
    void testShopInitializer_GenerateShops() throws SQLException {
        faker = new Faker(new Locale("uk"));
        Connection connection = mock(Connection.class);
        PreparedStatement addCategory = mock(PreparedStatement.class);

        DBInitializer.ShopsInitializer productsInitializer =
                new DBInitializer(connector, properties, faker).new ShopsInitializer(connection);
        assertDoesNotThrow(() -> {
            productsInitializer.generateShops(155, addCategory);
        });
        verify(addCategory, times(155)).addBatch();
        verify(addCategory, times(2)).executeBatch();
        faker = mock(Faker.class);
    }
}