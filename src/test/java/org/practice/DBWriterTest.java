package org.practice;

import com.github.javafaker.Faker;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.practice.connection.DBConnector;
import org.practice.generators.ProductShopGenerator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DBWriterTest {

    private Properties properties;
    private static DBConnector connector;
    private static Connection connection;

    @BeforeAll
    static void setup() {
        connector = mock(DBConnector.class);
        connection = mock(Connection.class);
    }

    @Test
    void testDbWriter_GenerateRecords() throws SQLException {
        Properties properties = mock(Properties.class);
        int numOfShops = 100;
        int numOfProducts = 200;
        DBWriter.DBThread writer = new DBWriter(connector, properties).new DBThread(connection, 10555, numOfShops, numOfProducts);
        PreparedStatement recordStatement = mock(PreparedStatement.class);
        assertDoesNotThrow(() -> {
            writer.generateRecords(new ProductShopGenerator(numOfShops, numOfProducts), recordStatement);
        });
        verify(recordStatement, times(10555)).addBatch();
        verify(recordStatement, times(2)).executeBatch();
    }

    @Test
    void testDbWriter_GenerateRecords_ShouldThrow() throws SQLException {
        Properties properties = mock(Properties.class);
        int numOfShops = 100;
        int numOfProducts = 200;
        DBWriter.DBThread writer = new DBWriter(connector, properties).new DBThread(connection, 10555, numOfShops, numOfProducts);
        PreparedStatement recordStatement = mock(PreparedStatement.class);
        when(recordStatement.executeBatch()).thenThrow(new SQLException());
        assertThrows(RuntimeException.class, () -> {
            writer.generateRecords(new ProductShopGenerator(numOfShops, numOfProducts), recordStatement);
        });
        verify(recordStatement, times(10000)).addBatch();
        verify(recordStatement, times(1)).executeBatch();
    }
}