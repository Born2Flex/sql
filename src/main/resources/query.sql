SELECT c.category_name, SUM(amount) amount_of_products, sh.city || ', ' || sh.street || ' ' || sh.house address
FROM products p
         JOIN categories c ON p.category_id = c.category_id AND c.category_name = '{category}'
         JOIN product_shop ps ON p.product_id = ps.product_id
         JOIN shops sh ON ps.shop_id = sh.shop_id
GROUP BY c.category_id, sh.shop_id
ORDER BY SUM(amount) DESC
LIMIT 1;