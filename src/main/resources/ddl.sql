DROP TABLE IF EXISTS addresses, categories, shops, products, product_shop;

-- CREATE TABLE addresses (
--     address_id SERIAL PRIMARY KEY,
--     city       VARCHAR(20) NOT NULL,
--     street     VARCHAR(30) NOT NULL,
--     house      VARCHAR(10) NOT NULL
-- );

CREATE TABLE categories (
    category_id   SERIAL PRIMARY KEY,
    category_name VARCHAR(50) NOT NULL
);

CREATE TABLE products (
    product_id   SERIAL PRIMARY KEY,
    category_id  INTEGER REFERENCES categories (category_id),
--     shop_id INTEGER REFERENCES shops (shop_id),
    product_name VARCHAR(50) NOT NULL
);

CREATE TABLE shops (
    shop_id    SERIAL PRIMARY KEY,
    city       VARCHAR(40) NOT NULL,
    street     VARCHAR(40) NOT NULL,
    house      VARCHAR(10) NOT NULL
);


CREATE TABLE product_shop (
    product_shop_id SERIAL PRIMARY KEY,
    shop_id         INTEGER REFERENCES shops (shop_id) NOT NULL,
    product_id      INTEGER REFERENCES products (product_id) NOT NULL,
    amount          INTEGER NOT NULL
);

CREATE INDEX products_category_id_idx ON products (category_id);
CREATE INDEX product_shop_product_id_idx ON product_shop (product_id);
CREATE INDEX product_shop_shop_id_idx ON product_shop (shop_id);


