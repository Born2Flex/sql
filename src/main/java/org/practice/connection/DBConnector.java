package org.practice.connection;

import org.practice.properties.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnector {
    private static final Logger log = LoggerFactory.getLogger(DBConnector.class);
    private final Properties properties;

    public DBConnector(Properties properties) {
        this.properties = properties;
    }

    public Connection connect() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(properties.getProperty("url"),
                                                properties.getProperty("username"),
                                                properties.getProperty("password"));
            log.info("Connection to DB created successfully");
        } catch (SQLException e) {
            log.warn("Can't create connection to DB");
            throw new RuntimeException("Failed to create connection", e);
        }
        return connection;
    }
}
