package org.practice;

import com.github.javafaker.Faker;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.practice.connection.DBConnector;
import org.practice.dto.ProductShopRecord;
import org.practice.generators.ProductShopGenerator;
import org.practice.properties.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class DBWriter implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(DBWriter.class);
    public static final int BATCH_SIZE = 10_000;
    private final DBConnector connector;
    private final Properties properties;
    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public DBWriter(DBConnector connector, Properties properties) {
        this.connector = connector;
        this.properties = properties;
    }

    @Override
    public void run() {
        log.info("DB writer started");
        int numOfThreads = Integer.parseInt(properties.getProperty("numOfThreads"));
        int numOfShops = Integer.parseInt(properties.getProperty("numOfShops"));
        int numOfProducts = Integer.parseInt(properties.getProperty("numOfProducts"));
        ExecutorService executorService = Executors.newFixedThreadPool(numOfThreads);

        int numOfRecords = Integer.parseInt(properties.getProperty("numOfProductShopRecords"));
        int[] recordsPerThread = new int[numOfThreads];
        Arrays.fill(recordsPerThread, numOfRecords / numOfThreads);
        if (numOfRecords % numOfThreads != 0) {
            recordsPerThread[0] += numOfRecords % numOfThreads;
        }

        for (int i = 0; i < numOfThreads; i++) {
            executorService.execute(new DBThread(connector.connect(), recordsPerThread[i], numOfShops, numOfProducts));
        }
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.MINUTES)) {
                log.info("DB writer didn't stopped, shutting down");
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            log.warn("Exception occurred while trying to stop DBWriter: ", e);
            executorService.shutdownNow();
        }
        log.info("DB writer stopped");
        log.info("Total records processed: {}", numOfRecords);
    }

    class DBThread implements Runnable {
        private final Logger log = LoggerFactory.getLogger(DBWriter.DBThread.class);
        public String addProductShopSQL = "INSERT INTO product_shop (shop_id, product_id, amount) VALUES (?, ?, ?)";
        private Connection connection;
        private int numOfRecords;
        private int numOfShops;
        private int numOfProducts;

        public DBThread(Connection connection, int numOfRecords, int numOfShops, int numOfProducts) {
            this.connection = connection;
            this.numOfRecords = numOfRecords;
            this.numOfShops = numOfShops;
            this.numOfProducts = numOfProducts;
        }

        @Override
        public void run() {
            try {
                log.info("DBWriter thread started");
                PreparedStatement preparedStatement = connection.prepareStatement(addProductShopSQL);
                connection.setAutoCommit(false);
                ProductShopGenerator productShopGenerator = new ProductShopGenerator(numOfShops, numOfProducts);
                generateRecords(productShopGenerator, preparedStatement);
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            log.info("DBWriter thread stopped. Records processed: {}", numOfRecords);
        }

        public void generateRecords(ProductShopGenerator generator, PreparedStatement preparedStatement) {
            AtomicInteger counter = new AtomicInteger();
            Stream.generate(generator::generateProductShopRecord)
                    .filter(productShopRecord -> validator.validate(productShopRecord).isEmpty())
                    .limit(numOfRecords)
                    .forEach(productShopRecord ->
                            {
                                try {
                                    preparedStatement.setInt(1, productShopRecord.getShopId());
                                    preparedStatement.setInt(2, productShopRecord.getProductId());
                                    preparedStatement.setInt(3, productShopRecord.getAmount());
                                    preparedStatement.addBatch();
                                    counter.incrementAndGet();
                                    if (counter.get() % BATCH_SIZE == 0) {
                                        preparedStatement.executeBatch();
                                        connection.commit();
                                        log.debug("Batch executed in ProductShop table");
                                    }
                                } catch (SQLException e) {
                                    log.error("SQLException occurred while generating ProductShop record", e);
                                    throw new RuntimeException(e);
                                }
                            }
                    );
            try {
                preparedStatement.executeBatch();
                connection.commit();
            } catch (SQLException e) {
                log.error("SQLException occurred while executing last batch in categories table", e);
                throw new RuntimeException(e);
            }
        }
    }
}
