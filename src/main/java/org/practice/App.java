package org.practice;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.time.StopWatch;
import org.practice.connection.DBConnector;
import org.practice.properties.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
public class App {
    private static final Logger log = LoggerFactory.getLogger(App.class);
    private static final String CATEGORY_BY_DEFAULT = "Design";
    private static final String QUERY_FILENAME = "query.sql";

    public static void main(String[] args) throws SQLException {
        Properties properties = new PropertiesLoader().loadProperties();
        Faker faker = new Faker(new Locale("uk"));
        DBConnector connector = new DBConnector(properties);
//        InputStream input = classLoader.getResourceAsStream(CONFIG_FILENAME);
//        String path1 = new File(ClassLoader.getSystemClassLoader().getResource("ddl.sql").getFile()).toPath().toString();
//        String path = ClassLoader.getSystemClassLoader().getResource("ddl.sql").getPath();
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        DBInitializer dbInitializer = new DBInitializer(connector, properties, faker);
        dbInitializer.run();
        DBWriter writer = new DBWriter(connector, properties);
        writer.run();
        stopWatch.stop();
        int numOfRecords = Integer.parseInt(properties.getProperty("numOfProductShopRecords"));
        double avg = (double) numOfRecords / stopWatch.getTime(TimeUnit.SECONDS);
        log.info("Time: {}s, WPS: {}", stopWatch.getTime(TimeUnit.SECONDS), avg);

        String param = null;
        if (args.length != 0) {
            log.info("Using category from command line: {}", args[0]);
            param = args[0];
        } else {
            log.info("Using category by default: {}", CATEGORY_BY_DEFAULT);
            param = CATEGORY_BY_DEFAULT;
        }
        executeQueryAndPrintResult(connector.connect(), readQueryFromFile(QUERY_FILENAME, param));
    }

    private static String readQueryFromFile(String filePath, String category) {
        ClassLoader classLoader = App.class.getClassLoader();
        InputStream input = classLoader.getResourceAsStream(filePath);
        StringBuilder query = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
            String line;
            while ((line = reader.readLine()) != null) {
                query.append(line).append("\n");
            }
            return query.toString().replace("{category}", category);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void executeQueryAndPrintResult(Connection connection, String query) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(query)) {
                if (resultSet.next()) {
                    log.info("Category: {}, Number of products: {}, Shop address: {}",
                            resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3));
                } else {
                    log.error("Query result is empty");
                }
            }
        }
    }
}
