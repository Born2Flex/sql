package org.practice.generators;

import com.github.javafaker.Faker;
import org.practice.dto.Product;

import java.util.Random;

public class ProductGenerator {
    private final Faker faker;
    private final int numOfCategories;
    private static final Random random = new Random();

    public ProductGenerator(Faker faker, int numOfCategories) {
        this.faker = faker;
        this.numOfCategories = numOfCategories;
    }

    public Product generateProduct() {
        Product product = new Product();
        product.setName(faker.commerce().productName());
        product.setCategoryId(random.nextInt(numOfCategories) + 1);
        return product;
    }
}
