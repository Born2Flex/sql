package org.practice.generators;

import org.practice.dto.ProductShopRecord;

import java.util.Random;

public class ProductShopGenerator {
    private int numOfShops;
    private int numOfProducts;
    private static final Random random = new Random();
    private static final int MAX_PRODUCT_AMOUNT = 10_000;
    public ProductShopGenerator(int numOfShops, int numOfProducts) {
        this.numOfShops = numOfShops;
        this.numOfProducts = numOfProducts;
    }

    public ProductShopRecord generateProductShopRecord() {
        ProductShopRecord record = new ProductShopRecord();
        record.setProductId(random.nextInt(numOfProducts) + 1);
        record.setShopId(random.nextInt(numOfShops) + 1);
        record.setAmount(random.nextInt(MAX_PRODUCT_AMOUNT) + 1);
        return record;
    }
}
