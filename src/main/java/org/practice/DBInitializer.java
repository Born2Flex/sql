package org.practice;

import com.github.javafaker.Faker;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.practice.connection.DBConnector;
import org.practice.generators.ProductGenerator;
import org.practice.generators.ShopGenerator;
import org.practice.properties.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class DBInitializer implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(DBInitializer.class);
    private static final String DDL_FILENAME = "ddl.sql";
    public static final int BATCH_SIZE = 100;
    private final DBConnector connector;
    private final Faker faker;
    private final Properties properties;
    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public DBInitializer(DBConnector connector, Properties properties, Faker faker) {
        this.connector = connector;
        this.properties = properties;
        this.faker = faker;
    }

    @Override
    public void run() {
        log.info("DB initializer started");
        ClassLoader classLoader = App.class.getClassLoader();
        InputStream input = classLoader.getResourceAsStream(DDL_FILENAME);
//            String text = new BufferedReader(
//                    new InputStreamReader(input, StandardCharsets.UTF_8))
//                    .lines()
//                    .collect(Collectors.joining("\n"));

        ScriptRunner scriptRunner = new ScriptRunner(connector.connect());
        scriptRunner.setSendFullScript(true);
        scriptRunner.runScript(new BufferedReader(new InputStreamReader(input)));
//            scriptRunner.runScript(new FileReader(path));
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new ProductsInitializer(connector.connect()));
        executorService.execute(new ShopsInitializer(connector.connect()));
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.SECONDS)) {
                log.info("DB initializer didn't stopped, shutting down");
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            log.warn("Exception occurred while trying to stop DBInitializer: ", e);
            executorService.shutdownNow();
        }
        log.info("DB initializer stopped");
    }

    class ProductsInitializer implements Runnable {
        public String addCategorySQL = "INSERT INTO categories (category_name) VALUES (?)";
        public String addProductSQL = "INSERT INTO products (category_id, product_name) VALUES (?, ?)";
        private Connection connection;

        public ProductsInitializer(Connection connection) {
            this.connection = connection;
        }

        @Override
        public void run() {
            int numOfCategories = Integer.parseInt(properties.getProperty("numOfCategories"));
            int numOfProducts = Integer.parseInt(properties.getProperty("numOfProducts"));
            try {
                PreparedStatement categoriesPreparedStatement = connection.prepareStatement(addCategorySQL);
                PreparedStatement productsPreparedStatement = connection.prepareStatement(addProductSQL);
                connection.setAutoCommit(false);
                generateCategories(numOfCategories, categoriesPreparedStatement);
                generateProducts(numOfProducts, numOfCategories, productsPreparedStatement);
                categoriesPreparedStatement.close();
                productsPreparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                log.error("SQLException occurred while running DBInitializer", e);
                throw new RuntimeException(e);
            }
        }

        public void generateCategories(int numOfCategories, PreparedStatement preparedStatement) {
            AtomicInteger counter = new AtomicInteger();
            log.info("Generation of categories started");
            Stream.generate(() -> faker.company().industry())
                    .limit(numOfCategories - 1)
                    .forEach(category -> {
                                try {
                                    preparedStatement.setString(1, category);
                                    preparedStatement.addBatch();
                                    counter.incrementAndGet();
                                    if (counter.get() % BATCH_SIZE == 0) {
                                        preparedStatement.executeBatch();
                                        connection.commit();
                                        log.debug("Batch executed in Categories table");
                                    }
                                } catch (SQLException e) {
                                    log.error("SQLException occurred while generating categories", e);
                                    throw new RuntimeException(e);
                                }
                            }
                    );
            try {
                preparedStatement.setString(1, "Design");
                preparedStatement.addBatch();
                preparedStatement.executeBatch();
                connection.commit();
                log.info("Generation of categories stopped");
            } catch (SQLException e) {
                log.error("SQLException occurred while executing last batch in categories table", e);
                throw new RuntimeException(e);
            }
        }

        public void generateProducts(int numOfProducts, int numOfCategories, PreparedStatement preparedStatement) {
            AtomicInteger counter = new AtomicInteger();
            ProductGenerator productGenerator = new ProductGenerator(faker, numOfCategories);
            log.info("Generation of products started");
            Stream.generate(productGenerator::generateProduct)
                    .filter(product -> validator.validate(product).isEmpty())
                    .limit(numOfProducts)
                    .forEach(product -> {
                                try {
                                    preparedStatement.setInt(1, product.getCategoryId());
                                    preparedStatement.setString(2, product.getName());
                                    preparedStatement.addBatch();
                                    counter.incrementAndGet();
                                    if (counter.get() % BATCH_SIZE == 0) {
                                        preparedStatement.executeBatch();
                                        connection.commit();
                                        log.debug("Batch executed in Products table");
                                    }
                                } catch (SQLException e) {
                                    log.error("SQLException occurred while generating products", e);
                                    throw new RuntimeException(e);
                                }
                            }
                    );
            try {
                preparedStatement.executeBatch();
                connection.commit();
                log.info("Generation of products stopped");
            } catch (SQLException e) {
                log.error("SQLException occurred while executing last batch in products table", e);
                throw new RuntimeException(e);
            }
        }

    }

    class ShopsInitializer implements Runnable {
        public String addShopSQL = "INSERT INTO shops (city, street, house) VALUES (?, ?, ?)";
        private Connection connection;

        public ShopsInitializer(Connection connection) {
            this.connection = connection;
        }

        @Override
        public void run() {
            int numOfShops = Integer.parseInt(properties.getProperty("numOfShops"));
            PreparedStatement shopsPreparedStatement = null;
            try {
                shopsPreparedStatement = connection.prepareStatement(addShopSQL);
                connection.setAutoCommit(false);
                generateShops(numOfShops, shopsPreparedStatement);
                shopsPreparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                log.error("SQLException occurred while running ShopInitializer", e);
                throw new RuntimeException(e);
            }
        }

        public void generateShops(int numOfShops, PreparedStatement preparedStatement) {
            AtomicInteger counter = new AtomicInteger();
            ShopGenerator productGenerator = new ShopGenerator(faker);
            log.info("Generation of shops started");
            Stream.generate(productGenerator::generateShop)
                    .limit(numOfShops)
                    .forEach(product -> {
                                try {
                                    preparedStatement.setString(1,product.getCity());
                                    preparedStatement.setString(2,product.getStreet());
                                    preparedStatement.setString(3,product.getHouseNum());
                                    preparedStatement.addBatch();
                                    counter.incrementAndGet();
                                    if (counter.get() % BATCH_SIZE == 0) {
                                        preparedStatement.executeBatch();
                                        connection.commit();
                                        log.debug("Batch executed in Shops table");
                                    }
                                } catch (SQLException e) {
                                    log.error("SQLException occurred while generating shops", e);
                                    throw new RuntimeException(e);
                                }
                            }
                    );
            try {
                preparedStatement.executeBatch();
                connection.commit();
                log.info("Generation of shops stopped");
            } catch (SQLException e) {
                log.error("SQLException occurred while executing last batch in shops table", e);
                throw new RuntimeException(e);
            }
        }
    }
}
