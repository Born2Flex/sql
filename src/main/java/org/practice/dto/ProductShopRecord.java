package org.practice.dto;

import jakarta.validation.constraints.Min;

public class ProductShopRecord {
    private int shopId;
    private int productId;
    @Min(value = 1000, message = "Amount of products can't be less than 1000!")
    private int amount;

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getShopId() {
        return shopId;
    }

    public int getProductId() {
        return productId;
    }

    public int getAmount() {
        return amount;
    }
}
