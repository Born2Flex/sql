package org.practice.dto;

import jakarta.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Length;

public class Product {
    @Pattern(regexp = ".*а.*", message = "Ukrainian word must contain 'а'")
    @Length(min = 5, max = 25, message = "Length of product name should be in range from 5 to 25")
    private String name;
    private int categoryId;

    public Product() {
    }

    public Product(String name, int categoryId) {
        this.name = name;
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
